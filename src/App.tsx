import { FC, useCallback, useMemo, useState } from 'react';
import './App.css';
import { Student } from './types/types';
import { Button, Modal } from 'antd';
import { StudentTable } from './components/StudentTable';
import { StudentModal } from './components/StudentModal';
import { DATA, EMPTY_EDITING_STUDENT } from './constants';
import React from 'react';

export const App: FC = () => {
  
  const [students, setStudents] = useState<Student[]>(DATA);
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [editingStudent, setEditingStudent] = useState<Student>(EMPTY_EDITING_STUDENT);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const isDisabled = useMemo((): boolean => !editingStudent?.fullName
    || !editingStudent?.groupNumber
    || !editingStudent?.courseNumber,
    [editingStudent?.fullName, editingStudent?.groupNumber, editingStudent?.courseNumber]
  )

  const handleAdd = useCallback((): void => {
    if (isDisabled) {
      return;
    }

    const newStudent: Student = {
      ...editingStudent,
      id: Date.now()
    }

    setStudents((prevState: Student[]) => [...prevState, newStudent]);
    setIsOpen(false);
    resetEditing();
  }, [isDisabled, editingStudent])

  const onDeleteStudent = useCallback((record: Student): void => {
    Modal.confirm({
      title: 'Вы уверены, что хотите удалить запись о клиенте?',
      okText: 'Удалить',
      cancelText: 'Отмена',
      onOk: () => {
        setStudents((prevState: Student[]) => {
          return prevState.filter((student: Student) => student.id !== record.id);
        })
      }
    });
  }, [])

  const onEditStudent = useCallback((record: Student): void => {
    setIsEditing(true);
    setIsOpen(true);
    setEditingStudent(record);
  }, [])

  const handleEdit = useCallback((): void => {
    if (isDisabled) {
      return;
    }
    setStudents((prevState: Student[]) => {
      return prevState.map((student: Student) => {
        if (student.id === editingStudent.id) {
          return editingStudent;
        } else {
          return student;
        }
      })
    });
    setIsOpen(false);
    resetEditing();
  }, [isDisabled, editingStudent])

  const resetEditing = useCallback((): void => {
    setIsEditing(false);
    setEditingStudent(EMPTY_EDITING_STUDENT);
  }, [])

  const handleCancel = useCallback((): void => {
    setIsOpen(false);
    resetEditing();
  }, [])

  const openOnAdd = useCallback((): void => {
    setIsOpen(true);
    setIsEditing(false);
  }, [])

  return (
    <div className='app'>
      <Button className='app__add-button' onClick={openOnAdd}>Добавить студента</Button>
      <StudentTable
        dataSource={students}
        onEditStudent={onEditStudent}
        onDeleteStudent={onDeleteStudent}
      />
      <StudentModal
        isEditing={isEditing}
        isOpen={isOpen}
        handleCancel={handleCancel}
        handleAdd={handleAdd}
        handleEdit={handleEdit}
        isButtonDisabled={isDisabled}
        editingStudent={editingStudent}
        setEditingStudent={setEditingStudent}
      />
    </div>
  );
}