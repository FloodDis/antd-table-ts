import { Form, Input } from "antd";
import { FC, memo, useEffect } from "react";
import { ParameterType, Student } from "../../types/types";
import React from "react";

interface TableFormProps {
    /** Редактируемый студент */
    editingStudent: Student;

    /** Сеттер состояния редактируемого студента */
    setEditingStudent: (value: Student | ((prevState: Student) => Student)) => void;

    /** Флаг, определяющий режим работы модального окна:
     * true - режим редактирования, false - режим добавления 
     * */
    isEditing: boolean;
}

export const TableForm: FC<TableFormProps> = memo(({ editingStudent, setEditingStudent, isEditing }) => {

    const [form] = Form.useForm();

    useEffect(() => {
        updateForm(isEditing);
    }, [isEditing]);

    useEffect(() => {
        updateValues();
    }, [editingStudent]);

    const updateValues = (): void => {
        form.setFieldsValue({
            fullName: editingStudent?.fullName,
            groupNumber: editingStudent?.groupNumber,
            courseNumber: editingStudent?.courseNumber
        })
    }

    const updateForm = (isEditing: boolean): void => {
        isEditing
            ? updateValues()
            : form.setFieldsValue({
                fullName: '',
                groupNumber: '',
                courseNumber: ''
            })
    }

    const onChange = (e: React.ChangeEvent<HTMLInputElement>, valueName: string): void => {
        setEditingStudent((prevState: Student) => { return { ...prevState, [valueName]: e.target.value } })
    }

    return (
        <Form form={form}>
            <Form.Item rules={
                [
                    {
                        required: true,
                        message: 'Необходимо ввести полное имя'
                    }
                ]}
                label='Полное имя'
                name='fullName'
                initialValue={editingStudent?.fullName}
            >
                <Input onChange={(e) => onChange(e, ParameterType.FullName)} placeholder="Полное имя" />
            </Form.Item>
            <Form.Item rules={
                [
                    {
                        required: true,
                        message: 'Необходимо ввести номер группы'
                    }
                ]}
                label='Номер группы'
                name='groupNumber'
                initialValue={editingStudent?.groupNumber}
            >
                <Input onChange={(e) => onChange(e, ParameterType.GroupNumber)} placeholder="Номер группы" />
            </Form.Item>
            <Form.Item rules={
                [
                    {
                        required: true,
                        message: 'Необходимо ввести курс'
                    }
                ]}
                label='Курс'
                name='courseNumber'
                initialValue={editingStudent?.courseNumber}
            >
                <Input onChange={(e) => onChange(e, ParameterType.CourseNumber)} placeholder="Курс" />
            </Form.Item>
        </Form>
    )
});