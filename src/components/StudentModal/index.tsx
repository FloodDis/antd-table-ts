import { Modal } from 'antd';
import { FC, memo } from 'react';
import { Student } from '../../types/types';
import { TableForm } from '../TableForm';
import React from 'react';

interface StudentModalProps {
    /** Флаг, определяющий режим работы модального окна:
     * true - режим редактирования, false - режим добавления
     */
    isEditing: boolean;

    /** Флаг, определяющий открыто ли модальное окно:
     * true - окно открыто, false - окно закрыто
     */
    isOpen: boolean;

    /** Обрабатывает нажатие на кнопку 'Отмена' */
    handleCancel: () => void;

    /** Обрабатывает добавление новой записи в таблицу */
    handleAdd: () => void;

    /** Обрабатывает редактирование существующей записи в таблице */
    handleEdit: () => void;

    /** Флаг, определяющий отключена ли кнопка добавления/сохранеия записи в таблицу:
     * true - кнопка отключена, false - кнопка включена
    */
    isButtonDisabled: boolean;

    /** Редактируемый студент  */
    editingStudent: Student;

    /** Сеттер состояния редактируемого студента */
    setEditingStudent: (value: Student | ((prevState: Student) => Student)) => void;
}

export const StudentModal: FC<StudentModalProps> = memo(({ isEditing, isOpen, handleCancel,
    handleAdd, handleEdit, isButtonDisabled, editingStudent, setEditingStudent }) => {
    return (
        <Modal
            title={
                isEditing
                    ? 'Редактирование записи о студенте'
                    : 'Добавление записи о студенте'
            }
            open={isOpen}
            okText={
                isEditing
                    ? 'Сохранить'
                    : 'Создать'
            }
            cancelText='Отмена'
            onOk={
                isEditing
                    ? handleEdit
                    : handleAdd
            }
            okButtonProps={{ disabled: isButtonDisabled }}
            onCancel={handleCancel}
        >
            <TableForm
                editingStudent={editingStudent}
                setEditingStudent={setEditingStudent}
                isEditing={isEditing}
            />
        </Modal>
    )
});