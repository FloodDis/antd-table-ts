import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Table } from "antd";
import React, { FC, memo } from "react";
import { Student } from "../../types/types";
import './styles.css'

interface Column {
    /** Ключ столбца */
    key: string;

    /** Название солбца */
    title: string;

    /** Название поля, из которого будут браться данные для отображения в столбце */
    dataIndex?: string;

    /** Рендерит react-компоненты внутри ячейки таблицы */
    render?: (record: Student) => React.ReactElement
}

interface StudentTableProps {
    /** Источник данных для таблицы */
    dataSource: Student[];

    /** Обрабатывает нажатие на кнопку редактирования существующей записи о студенте */
    onEditStudent: (record: Student) => void;

    /** Обрабатывает нажатие на кнопку удаления записи о студенте */
    onDeleteStudent: (record: Student) => void;
}

export const StudentTable: FC<StudentTableProps> = memo(({ dataSource, onEditStudent, onDeleteStudent }) => {

    const columns: Column[] = [
        {
            key: 'fullName',
            title: 'Полное имя',
            dataIndex: 'fullName'
        },
        {
            key: 'groupNumber',
            title: 'Номер группы',
            dataIndex: 'groupNumber'
        },
        {
            key: 'courseNumber',
            title: 'Номер курса',
            dataIndex: 'courseNumber'
        },
        {
            key: 'actions',
            title: 'Действие',
            render: (record: Student) => {
                return (
                    <>
                        <EditOutlined className="edit-button" onClick={() => onEditStudent(record)} />
                        <DeleteOutlined className="delete-button" onClick={() => onDeleteStudent(record)} />
                    </>
                )
            }
        }
    ]

    return (
        <Table pagination={false} columns={columns} dataSource={dataSource} />
    )
});