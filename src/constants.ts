import { Student } from "./types/types";

export const DATA: Student[] = [
    {
        id: 1,
        fullName: 'Сеченов Владислав Вадимович',
        groupNumber: '580-2',
        courseNumber: '4'
    },
    {
        id: 2,
        fullName: 'Иванов Иван Иванович',
        groupNumber: '580-2',
        courseNumber: '3'
    }
]

export const EMPTY_EDITING_STUDENT: Student = {
    id: 0,
    fullName: '',
    groupNumber: '',
    courseNumber: ''
}