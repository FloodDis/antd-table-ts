export interface Student {
    /** Полное имя студента */
    fullName: string;

    /** Номер группы */
    groupNumber: string;

    /** Номер курса */
    courseNumber: string;

    /** Идентификатор студента */
    id?: number;
}

export enum ParameterType {
    /** Полное имя студента */
    FullName='fullName',

    /** Номер группы */
    GroupNumber='groupNumber',

    /** Номер курса */
    CourseNumber='courseNumber'
}